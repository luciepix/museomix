//
//  AudioManager.h
//  OffAxisPerspectiveProjection
//
//  Created by Elie Zananiri on 12-05-07.
//  Copyright (c) 2012 Departement. All rights reserved.
//

#pragma once

#define kNumVoiceOvers 5

#include "ofMain.h"
#include "Fader.h"

//--------------------------------------------------------------
//--------------------------------------------------------------
class AudioManager 
{    
    public:
        static AudioManager* getInstance();
        static void destroyInstance();
        
        AudioManager();
        ~AudioManager();
    
		void	setup();
        void    update();
    
        void    startTrameSonore();
        void    stopTrameSonore();
        void    fadeBackdrop(float volume, float duration);

    private:
        static AudioManager* instance;
        
        Fader*  _backdropFader;

        ofSoundPlayer _trameSonore;
    
        //ofSoundPlayer _emptyRoomAnimInSound;
        //ofSoundPlayer _emptyRoomAnimOutSound;
    
        // ----
        Fader*  _voiceOverFader;

        void fadeVoiceOver(float volume, float duration);

        //int _voLang, _voView;
        //ofSoundPlayer* _voiceOvers[MENU_LANGUAGE_COUNT][kNumVoiceOvers];
    
        // ---- button fx
        //ofSoundPlayer _menuButtonFx1;
        //ofSoundPlayer _menuButtonFx2;
};
