#pragma once

#include "ofMain.h"
#include "ofVideoPlayer.h"
#include "ofEvents.h"
#include "ofxTimer.h"
#include "kinect\KinectFactory.h"
#include "Storyboard.h"
#include "audio\AudioManager.h"

class KinectNui;

class PageManager
{
public:
	PageManager(void);
	~PageManager(void);

	void setup();
	void update();
	void reset();
	
	ofTexture getBookTexture();
	ofImage* getLeftPage();
	ofImage* getRightPage();
	
	typedef enum CensorState {
		CENSOR_NONE,
		CENSOR_LITTLE,
		CENSOR_MORE,
		CENSOR_ALL,
		CENSOR_NB_STAGES
	} CensorState_;

	typedef enum PageDetectionState {
		PAGE_UP,
		PAGE_DOWN
	} PageDetectionState_;

	typedef enum VisitorState {
		VISITOR_NONE,
		VISITOR_PRESENT
	} VisitorState_;

	typedef enum StoryState {
		STORY_NONE,
		STORY_CENSORSHIP,
		STORY_QUESTION,
		STORY_NOW,
		STORY_STANDBY
	} StoryState_;

	bool _hasBookTexture;

private:

	int _calibX;
	int _calibY;
	int _calibMin;
	int _calibMax;

	AudioManager _audioManager;

	ofTexture _currentBookTexture;
	ofImage _image2013;

	ofImage _imagesNow[15];
	int _imagesNowNb;
	int _imagesNowIdx;

	void startExperience();

	KinectNui *_kinect;
	
	int _storyNbPages;
	int _currentPage;
	ofImage *_currentLeftPage;
	ofImage *_currentRightPage;

	ofImage _leftPageImages[6][CENSOR_NB_STAGES];
	ofImage _rightPageImages[6][CENSOR_NB_STAGES];

	StoryState _storyState;
	CensorState _censorState;
	PageDetectionState _pageState;
	VisitorState _visitorState;

	// Gestion des timings du scenario
	// Tous les temps sont en s
	int _timeCensorNone;
	int _timeCensorLittle;
	int _timeCensorMore;
	int _timeCensorAll;
	int _timeQuestion;
	int _timeNewspaper;
	int _timeStandby;

	ofxTimer _timerStoryboard;
	void onStoryboardTimerReached(const void* sender, ofEventArgs& args);   

	//void startLittleCensor(const void* sender, ofEventArgs& args);   
	//void startMoreCensor(const void* sender, ofEventArgs& args); 
	//void startAllCensor(const void* sender, ofEventArgs& args); 
	//void startQuestion(const void* sender, ofEventArgs& args); 
	//void startNewspapers(const void* sender, ofEventArgs& args); 
	//void nextNewspaper(const void* sender, ofEventArgs& args);
	//void resetExperience(const void* sender, ofEventArgs& args);

};

