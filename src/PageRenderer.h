#pragma once

#include "ofMain.h"
#include "ofFbo.h"

class PageManager;

class PageRenderer
{
public:
	PageRenderer(void);
	~PageRenderer(void);

	void setup();
	void update();
	void draw();

private:
	PageManager *_pageManager;

	float _matrixLeft[16];
	float _matrixRight[16];

	ofImage *_leftImg;
	ofImage *_rightImg;
	ofTexture _bookTex;

	ofPoint _mappingLeft[4];
	ofPoint _mappingRight[4];
	ofPoint _mappingBook[4];

	void updateHomographies();

};

