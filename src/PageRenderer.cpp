#include "PageRenderer.h"
#include "PageManager.h"
#include "homography\homography.h"

PageRenderer::PageRenderer(void) {
	_pageManager = new PageManager();
}


PageRenderer::~PageRenderer(void) {
	delete _pageManager;
}


void PageRenderer::setup() {

	_pageManager->setup();

	_leftImg = _pageManager->getLeftPage();
	_rightImg = _pageManager->getRightPage();
	_bookTex = _pageManager->getBookTexture();

	//_leftImg.loadImage(ofToDataPath("pages/Page5-gauche-746x1280.png", true));
	//_rightImg.loadImage(ofToDataPath("pages/Page5-droite-746x1280.png", true));
	
	int mtop = 10;
	int mside = 71;
	int mbetween = 10;

	int w = 436;
	int h = 748;

	_mappingLeft[0] = ofPoint(323, 308);
	_mappingLeft[1] = ofPoint(541, 314);
	_mappingLeft[2] = ofPoint(533, 664);
	_mappingLeft[3] = ofPoint(313, 662);
	
	_mappingRight[0] = ofPoint(543, 316);
	_mappingRight[1] = ofPoint(763, 325);
	_mappingRight[2] = ofPoint(755, 678);
	_mappingRight[3] = ofPoint(537, 665);
	
	_mappingBook[0] = ofPoint(323, 308);
	_mappingBook[1] = ofPoint(763, 325);
	_mappingBook[2] = ofPoint(755, 678);
	_mappingBook[3] = ofPoint(313, 662);

	//updateHomographies();
}

void PageRenderer::update() {
	_pageManager->update();
	_leftImg = _pageManager->getLeftPage();
	_rightImg = _pageManager->getRightPage();
	_bookTex = _pageManager->getBookTexture();
}

void PageRenderer::draw() {

    ofClear(.0f, .0f, .0f, .0f);

	//
	// PAGE GAUCHE
	//
    ofSetColor(255, 255, 255, 255);
    
	int w,h;

	if (_leftImg) {
		w = _leftImg->getWidth();
		h = _leftImg->getHeight();
    
		ofPushMatrix();
		//glMultMatrixf(_matrixLeft);
		{
			glColor4f(1., 1., 1., 1.);
			_leftImg->bind();
			glBegin(GL_POLYGON);
			{
				glTexCoord2f(0,0);
				glVertex2f(_mappingLeft[0].x, _mappingLeft[0].y);
            
				glTexCoord2f(w, 0);
				glVertex2f(_mappingLeft[1].x, _mappingLeft[1].y);
            
				glTexCoord2f(w, h);
				glVertex2f(_mappingLeft[2].x, _mappingLeft[2].y);
            
				glTexCoord2f(0, h);
				glVertex2f(_mappingLeft[3].x, _mappingLeft[3].y);
			}
			glEnd();
			_leftImg->unbind();
		}
		ofPopMatrix();
	}
	//
	// PAGE DROITE
	//
	if (_rightImg) {
		ofSetColor(255, 255, 255, 255);

		w = _rightImg->getWidth();
		h = _rightImg->getHeight();
    
		ofPushMatrix();
		//glMultMatrixf(_matrixRight);
		{
			glColor4f(1., 1., 1., 1.);
			_rightImg->bind();
			glBegin(GL_POLYGON);
			{
				glTexCoord2f(0,0);
				glVertex2f(_mappingRight[0].x, _mappingRight[0].y);
            
				glTexCoord2f(w, 0);
				glVertex2f(_mappingRight[1].x, _mappingRight[1].y);
            
				glTexCoord2f(w, h);
				glVertex2f(_mappingRight[2].x, _mappingRight[2].y);
            
				glTexCoord2f(0, h);
				glVertex2f(_mappingRight[3].x, _mappingRight[3].y);
			}
			glEnd();
			_rightImg->unbind();
		}
		ofPopMatrix();
	}

	//
	// PAGE COMPLETE
	//
	if (_bookTex.isAllocated()) {
		ofSetColor(255, 255, 255, 255);

		w = _bookTex.getWidth();
		h = _bookTex.getHeight();
    
		ofPushMatrix();
		//glMultMatrixf(_matrixRight);
		{
			glColor4f(1., 1., 1., 1.);
			_bookTex.bind();
			glBegin(GL_POLYGON);
			{
				glTexCoord2f(0,0);
				glVertex2f(_mappingBook[0].x, _mappingBook[0].y);
            
				glTexCoord2f(w, 0);
				glVertex2f(_mappingBook[1].x, _mappingBook[1].y);
            
				glTexCoord2f(w, h);
				glVertex2f(_mappingBook[2].x, _mappingBook[2].y);
            
				glTexCoord2f(0, h);
				glVertex2f(_mappingBook[3].x, _mappingBook[3].y);
			}
			glEnd();
			_bookTex.unbind();
		}
		ofPopMatrix();
	}
	
	//ofSetColor(255);
	//ofFill();
	//ofRect(0,0,1024,100);
}

void PageRenderer::updateHomographies() {

	int w = ofGetWidth();
	int h = ofGetHeight();

    ofPoint src[4];
    ofPoint dst[4];
    
    src[0].set(0, 0, 0);
    src[1].set(w, 0, 0);
	src[2].set(w, h, 0);
	src[3].set(0, h, 0);
    
    dst[0].set(_mappingLeft[0].x, _mappingLeft[0].y, 0);
    dst[1].set(_mappingLeft[1].x, _mappingLeft[1].y, 0);
    dst[2].set(_mappingLeft[2].x, _mappingLeft[2].y, 0);
    dst[3].set(_mappingLeft[3].x, _mappingLeft[3].y, 0);
    
    findHomography(src, dst, _matrixLeft);
	
    dst[0].set(_mappingRight[0].x, _mappingRight[0].y, 0);
    dst[1].set(_mappingRight[1].x, _mappingRight[1].y, 0);
    dst[2].set(_mappingRight[2].x, _mappingRight[2].y, 0);
    dst[3].set(_mappingRight[3].x, _mappingRight[3].y, 0);
	
    findHomography(src, dst, _matrixRight);
}