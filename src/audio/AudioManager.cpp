//
//  AudioManager.cpp
//  OffAxisPerspectiveProjection
//
//  Created by Elie Zananiri on 12-05-07.
//  Copyright (c) 2012 Departement. All rights reserved.
//

#include "AudioManager.h"

//#include "LedController.h"
//#include "ControlsFactory.h"

//#define kAssetsAudioPath        "assets/audio/"
//#define kAssetsVoiceOverPath    "assets/audio/voiceover/"
//#define kAssetsBackDropPath     "assets/audio/back_drop/"

//--------------------------------------------------------------
AudioManager* AudioManager::instance = NULL;

//--------------------------------------------------------------
AudioManager* AudioManager::getInstance() 
{
    if (!instance) {
        instance = new AudioManager();
    }
    return instance;
}

//--------------------------------------------------------------
void AudioManager::destroyInstance() 
{
    if (instance) {
        delete instance;
    }
}

//--------------------------------------------------------------
AudioManager::AudioManager()
{

    /*
    _backdropTrack.loadSound(string(kAssetsBackDropPath).append("Backdrop_Mstr.wav"), true);
    _backdropTrack.setLoop(true);
    
    _airhornSound.loadSound(string(kAssetsAudioPath).append("air-horn.mp3"));
    _airhornSound.setMultiPlay(true);
    
    _neonSound.loadSound(string(kAssetsAudioPath).append("neon_bass_01.wav"));
    _neonSound.setVolume(ControlsFactory::controlsGeneral()->neonAnimMinVolume());
    _neonSound.setMultiPlay(true);
    
    // load voice over sound files
    _voiceOvers[MENU_FRENCH][0] = new ofSoundPlayer();
    _voiceOvers[MENU_FRENCH][0]->loadSound(string(kAssetsVoiceOverPath).append("Jerome_Intro_Mstr.wav"));
    _voiceOvers[MENU_FRENCH][1] = new ofSoundPlayer();
    _voiceOvers[MENU_FRENCH][1]->loadSound(string(kAssetsVoiceOverPath).append("Jerome_Tableau1_Mstr.wav"));
    _voiceOvers[MENU_FRENCH][2] = new ofSoundPlayer();
    _voiceOvers[MENU_FRENCH][2]->loadSound(string(kAssetsVoiceOverPath).append("Jerome_Tableau2_Mstr.wav"));
    _voiceOvers[MENU_FRENCH][3] = new ofSoundPlayer();
    _voiceOvers[MENU_FRENCH][3]->loadSound(string(kAssetsVoiceOverPath).append("Jerome_Tableau3_Mstr.wav"));
    _voiceOvers[MENU_FRENCH][4] = new ofSoundPlayer();
    _voiceOvers[MENU_FRENCH][4]->loadSound(string(kAssetsVoiceOverPath).append("Jerome_Tableau4_Mstr.wav"));
    
    _voiceOvers[MENU_ENGLISH][0] = new ofSoundPlayer();
    _voiceOvers[MENU_ENGLISH][0]->loadSound(string(kAssetsVoiceOverPath).append("Jason_Intro_Mstr.wav"));
    _voiceOvers[MENU_ENGLISH][1] = new ofSoundPlayer();
    _voiceOvers[MENU_ENGLISH][1]->loadSound(string(kAssetsVoiceOverPath).append("Jason_Tableau1_Mstr.wav"));
    _voiceOvers[MENU_ENGLISH][2] = new ofSoundPlayer();
    _voiceOvers[MENU_ENGLISH][2]->loadSound(string(kAssetsVoiceOverPath).append("Jason_Tableau2_Mstr.wav"));
    _voiceOvers[MENU_ENGLISH][3] = new ofSoundPlayer();
    _voiceOvers[MENU_ENGLISH][3]->loadSound(string(kAssetsVoiceOverPath).append("Jason_Tableau3_Mstr.wav"));
    _voiceOvers[MENU_ENGLISH][4] = new ofSoundPlayer();
    _voiceOvers[MENU_ENGLISH][4]->loadSound(string(kAssetsVoiceOverPath).append("Jason_Tableau4_Mstr.wav"));
    
    _voLang = MenuView::currLang();
    _voView = -1;
    
    // ----- Menu fx
    _menuButtonFx1.loadSound(string(kAssetsAudioPath).append("button-3.wav"));
    _menuButtonFx2.loadSound(string(kAssetsAudioPath).append("button-35.wav"));
    
    // ----
    _backdropFader = NULL;
    _voiceOverFader = NULL;

    _bDoStartViewVoiceOver = false;
    _viewVoiceOverToStart = 0;
    _bDoStartCTAVoiceOver = false;
    _bDoFadeOutCurrVoiceOver = false;
    _bDoTriggerNeonSound = false;
    _bDoTriggerMenuButtonSound = false;
    */
    ofLogVerbose() << "Audio successfully loaded!";
}

//--------------------------------------------------------------
AudioManager::~AudioManager()
{
	_trameSonore.unloadSound();

	/*
    _backdropTrack.unloadSound();
    _airhornSound.unloadSound();
    _neonSound.unloadSound();
    
    for (int lang = 0; lang < MENU_LANGUAGE_COUNT; lang++) {
        for (int n = 0; n < kNumVoiceOvers; n++) {
            delete _voiceOvers[lang][n];
        }
    }
        
    _menuButtonFx1.unloadSound();
    _menuButtonFx2.unloadSound();
    
    if (_backdropFader) delete _backdropFader;
    
    if (_voiceOverFader) delete _voiceOverFader;
	*/
}

void AudioManager::setup() {
	
    ofLog() << "Loading audio...";

	//string path = "C:\\Users\\Lucie\\Dev\\openFrameworks\\apps\\Pixiole\\museomix\\bin\\data\\son\\TrameProvisoire_1-2.aif";
	string path = ofToDataPath("son/TrameProvisoire_1-2.aif");
	_trameSonore.loadSound(path,false);
	_trameSonore.setLoop(false);
}

//--------------------------------------------------------------
void AudioManager::update()
{
	/*
    ofLogVerbose() << "------------ AudioManager: On entre dans update";
    
    // manage any sound triggering updates
    if (_bDoStartViewVoiceOver) {
        _bDoStartViewVoiceOver = false;

        triggerVoiceOverSound(_viewVoiceOverToStart);
    }
    else if (_bDoStartCTAVoiceOver) {
        _bDoStartCTAVoiceOver = false;

        // only trigger the CTA if no other voice over is already playing
        if (_voView == -1) {
            LedController::getInstance()->setLastFramePulsate(true);
            triggerVoiceOverSound(0);
        }
    }

    if (_bDoFadeOutCurrVoiceOver) {
        _bDoFadeOutCurrVoiceOver = false;

        fadeVoiceOver(.0f, 600.0f);
    }

    if (_bDoTriggerNeonSound) {
        _bDoTriggerNeonSound = false;

            _neonSound.play();
    }

    if (_bDoTriggerMenuButtonSound) {
        _bDoTriggerMenuButtonSound = false;

            _menuButtonFx1.play();
            //_menuButtonFx2.play();
    }
    
    // done dealing with flags, do some fader stuff

    if (_backdropFader) {
        _backdropFader->update();
            
        // set the backdrop volume
        _backdropTrack.setVolume(_backdropFader->getValue());
        
        // clean up if the fader is done
        if (!_backdropFader->isFading()) {
            if (_backdropTrack.getVolume() < 0.001f) {
                _backdropTrack.stop();
            }
            
            delete _backdropFader;
            _backdropFader = NULL;
        }
    }
    
    if (_voiceOverFader) {
        _voiceOverFader->update();
            
        if (_voView > -1) {
            // set the voice over volume
            _voiceOvers[_voLang][_voView]->setVolume(_voiceOverFader->getValue());
        }
        
        // clean up if the fader is done
        if (!_voiceOverFader->isFading()) {
            delete _voiceOverFader;
            _voiceOverFader = NULL;
        }
    }
    
    // manage the current voice over
    if (_voView > -1) {
        if (_voiceOvers[_voLang][_voView]->getVolume() < 0.001f) { 
            _voiceOvers[_voLang][_voView]->stop();
        }

        if (!_voiceOvers[_voLang][_voView]->getIsPlaying()) {
            // bring the neon sound volume back up
            _neonSound.setVolume(ControlsFactory::controlsGeneral()->neonAnimMaxVolume());
                
            // bring the backdrop track volume back up
            fadeBackdrop(ControlsFactory::controlsGeneral()->backDropMaxVolume(), 500.0f);

            // turn off the red LED pulse if it was on (IDLE mode)
            if (_voView == 0) {
                LedController::getInstance()->setLastFramePulsate(false);
            }

            _voView = -1;
        }
    }

    ofLogVerbose() << "------------ AudioManager: On sort de update";
	*/
}

#pragma mark - Main Back Drop
//--------------------------------------------------------------
void AudioManager::startTrameSonore()
{
    _trameSonore.play();
}

//--------------------------------------------------------------
void AudioManager::stopTrameSonore()
{
    _trameSonore.stop();
}

//--------------------------------------------------------------
void AudioManager::fadeBackdrop(float volume, float duration)
{
	/*
    ofLogVerbose() << "------------ On entre dans fadeBackdrop(float volume, float duration)" << endl;
    float currentVol;
    
	// get the current volume
    if (_backdropFader) {
        currentVol = _backdropFader->getValue();
        _backdropTrack.setVolume(currentVol);
        
        // stop the current fade
        delete _backdropFader;
        _backdropFader = NULL;
	}
    else {
        currentVol = _backdropTrack.getVolume();
    }
    
    if (currentVol < volume+0.001 && currentVol > volume-0.001) {
        // already at the target volume, abort!
        ofLogVerbose() << "------------ On sort de fadeBackdrop(float volume, float duration) target volume" << endl;
        return;
    }
	
	// build a new fade
	_backdropFader = new Fader(currentVol, volume, duration, FADE_EXP, true);
    
    // start the sound if necessary
    if (!_backdropTrack.getIsPlaying()) {
        _backdropTrack.play();
    }
    ofLogVerbose() << "------------ On sort de fadeBackdrop(float volume, float duration)" << endl;
	*/
}


//--------------------------------------------------------------
void AudioManager::fadeVoiceOver(float volume, float duration)
{
	/*
    ofLogVerbose() << "------------ On entre dans fadeVoiceOver(float volume, float duration)" << endl;
    if (_voView == -1) {
        ofLogVerbose() << "------------ On sort de fadeVoiceOver(float volume, float duration) !_currVoiceOver" << endl;
        return;
    }

    float currentVol = _voiceOvers[_voLang][_voView]->getVolume();

    if (_voiceOverFader) {
        delete _voiceOverFader;
        _voiceOverFader = NULL;
    }

    if (currentVol < volume+0.001 && currentVol > volume-0.001) {
        // already at the target volume, abort!
        ofLogVerbose() << "------------ On sort de fadeVoiceOver(float volume, float duration) targetvolume" << endl;
        return;
    }

	// build a new fade
	_voiceOverFader = new Fader(currentVol, volume, duration, FADE_EXP, true);

    // start the voice over if necessary
    if (!_voiceOvers[_voLang][_voView]->getIsPlaying()) {
        _voiceOvers[_voLang][_voView]->play();
    }
    ofLogVerbose() << "------------ On sort de fadeVoiceOver(float volume, float duration) targetvolume" << endl;
	*/
}
