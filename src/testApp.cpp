#include "kinect\KinectNui.h"
#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	
	ofSetFrameRate(30);
	ofSetDataPathRoot(".\\bin\\data\\");
	ofEnableAlphaBlending();
	ofHideCursor();

	_kinect = dynamic_cast<KinectNui*>(KinectFactory::kinect());

	// set up the Kinect capture
    if (!_kinect->init()) {
        ofLogFatalError() << "Unable to initialize Kinect!" << endl;
//        exit();
    }
    if (!_kinect->open()) {
        ofLogFatalError() << "Unable to open Kinect!" << endl;
//	     exit();
    }

    ofLogNotice() << "Kinect ready with depth = " << KinectFactory::kinect()->getDepthWidth() << "x" <<  KinectFactory::kinect()->getDepthHeight() << ", video = " << KinectFactory::kinect()->getVideoWidth() << "x" <<  KinectFactory::kinect()->getVideoHeight();
    


	_pageRenderer.setup();
}

void testApp::exit() {
	
	if (_kinect)
		_kinect->close();
}


//--------------------------------------------------------------
void testApp::update(){

	_kinect->update();
	_pageRenderer.update();
}

//--------------------------------------------------------------
void testApp::draw(){

	_pageRenderer.draw();

	if (_kinect->isFrameNew() && 0) {
		//_kinect->drawSkeleton(0,0,500,500);
		//_kinect->drawDepth(0, 0, 320, 240);

		ofSetColor(255);
		
		unsigned short depth = 0;
		ofVec3f pt3d(-1,-1,-1);
		if (mouseX < _kinect->getDepthWidth() && mouseY < _kinect->getDepthHeight()) {
			depth = _kinect->getDistanceAt(mouseX, mouseY);
			pt3d.set(_kinect->getWorldCoordinateAt(mouseX, mouseY));
		}
		//depth = depthPixels[_kinect->getDepthWidth()*120 + 160];

		string mouseString = "MOUSE: " + ofToString(mouseX) + " " + ofToString(mouseY);
		string depthString = "DEPTH: " + ofToString(depth);
		string depth3dString = "DEPTH: " + ofToString(pt3d.x) + " " + ofToString(pt3d.y) + " " + ofToString(pt3d.z);
		
		ofDrawBitmapString(mouseString, 20, 20, 0);
		ofDrawBitmapString(depthString, 20, 60, 0);
		ofDrawBitmapString(depth3dString, 20, 100, 0);
	}
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}