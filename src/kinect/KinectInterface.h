#ifndef DPT_KINECT_INTERFACE_H
#define DPT_KINECT_INTERFACE_H

#pragma once

#include "ofMain.h"

class KinectInterface
{
protected:
    ofMatrix4x4 _camVirtualPose;
    ofMatrix4x4 _camVirtualRotation;
    ofVec3f _camVirtualTranslation;

    bool _lockOnUser;

    //virtual void updateVirtualPose() = 0;

public:
    //virtual ~KinectInterface(void);

    // Device management functions
    virtual bool init() = 0;
    virtual bool open() = 0;
    virtual void close() = 0;

    virtual void update() = 0;

    virtual bool isFrameNew() = 0;

    // Video dimension
    virtual int getVideoWidth() = 0;
    virtual int getVideoHeight() = 0;
    virtual int getDepthWidth() = 0;
    virtual int getDepthHeight() = 0;

    // Motor functions
    virtual void setAngle(int angleInDegrees) = 0;
    virtual int getCurrentAngle() = 0;
    virtual int getTargetAngle() = 0;

    // RGB pixels
    virtual ofColor getColorAt(int x, int y) = 0;
    virtual ofColor getColorAt(const ofPoint & p) = 0;

    virtual ofColor getCalibratedColorAt(int depthX, int depthY) = 0;
    virtual ofColor getCalibratedColorAt(const ofPoint& depthPoint) = 0;

    // Depth data
    virtual ofVec3f getWorldCoordinateAt(int depthX, int depthY) = 0;

    virtual unsigned short getDistanceAt(int depthX, int depthY) = 0;
    virtual unsigned short getDistanceAt(const ofPoint& depthPoint) = 0;

    // Calibration data
    virtual void setVirtualTranslation(float x, float y, float z) = 0;
    virtual void setVirtualRotation(float x, float y, float z) = 0;

    // Label data
    virtual void disableUserSwitch() = 0;
    virtual void enableUserSwitch() = 0;
    virtual int getTrackedPlayer() = 0;
    virtual int getClosestPlayer() = 0;
    virtual float getTrackedPlayerDepthAt(int depthX, int depthY) = 0;
    virtual int getPlayerIndexAt(int depthX, int depthY) = 0;
    virtual int getPlayerIndexAt(const ofPoint& point) = 0;

    // Skeleton data
    virtual bool isFoundSkeleton() = 0;
    virtual void drawSkeleton(int x, int y, int w, int h) = 0;
    virtual const ofPoint* getSkeleton() = 0;
    virtual ofVec3f getHeadPosition() = 0;
    virtual ofVec3f getLeftWrist() = 0;
    virtual ofVec3f getRightWrist() = 0;
    virtual float getLeftArmElevation() = 0;
    virtual float getRightArmElevation() = 0;
	//virtual ofVec3f transformDepthToVirtualWorld(int x, int y) = 0;


    // Drawing functions
    virtual void drawRaw(float x, float y, float w, float h) = 0;
    virtual void drawDepth(float x, float y, float w, float h) = 0;

    virtual unsigned char* getDepthPixels() = 0;
};

#endif