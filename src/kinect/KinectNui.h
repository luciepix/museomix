#ifndef DPT_KINECT_NUI_H
#define DPT_KINECT_NUI_H

#pragma once

#ifdef WIN32

#include <time.h>
#include <Windows.h>
#include "KinectInterface.h"
#include "ofxKinectNui.h"
#include "ofxKinectNuiDraw.h"

class KinectNui : public KinectInterface
{
private:
    ofxKinectNui _kinect;
	ofxKinectNuiDrawSkeleton*	_skeletonDraw;
	ofxKinectNuiDrawTexture* _depthDraw;

    // Attention: 
    // les IDs des squelettes ne correspondent pas aux IDs des joueurs suivis.
    // Dans ofxKinectNui, il y a plus de joueurs qui peuvent etre suivis (max 8)
    // qu'il y a de squelettes (max 6).
    int _trackedSkeletonID;
    int _closestSkeletonID;
    int _highestScoreSkeletonID;
    int _trackedPlayerID;
    int _closestPlayerID;

    int _nbFrameToConsiderNotFound;
    int _nbFrameSkeletonNotFound;
    
    // Pour le smoothing des coordonnes des poignets
    ofVec3f _leftHand;
    ofVec3f _rightHand;
    ofVec3f _head;
    bool _doSmooth;

    //ofPoint** _skeletons;
    ofPoint* _skeletons[kinect::nui::SkeletonFrame::SKELETON_COUNT];
    //ofPoint _worldSkeleton[NUI_SKELETON_POSITION_COUNT];     // 20
    //ofPoint _virtualSkeleton[NUI_SKELETON_POSITION_COUNT];   // 20
    ofPoint _worldSkeletons[kinect::nui::SkeletonFrame::SKELETON_COUNT][NUI_SKELETON_POSITION_COUNT];     // 20
    ofPoint _virtualSkeletons[kinect::nui::SkeletonFrame::SKELETON_COUNT][NUI_SKELETON_POSITION_COUNT];   // 20
    float _skeletonWeights[kinect::nui::SkeletonFrame::SKELETON_COUNT];

    time_t _switchActivatedSince;
    bool _switchActivated;
    int _switchDelayInSeconds;

    //void updateTracking();
    //bool searchHighestScoreSkeleton();
    //bool searchClosestToScreenSkeleton();

    //void updateVirtualPose();
    //void updateVirtualSkeleton();
    //void updateWorldSkeleton();

    ofPoint transformSkeletonToDepthImage(ofVec3f joint);

public:
    KinectNui(void);
    ~KinectNui(void) {};

    // Device management functions
    virtual bool init();
    virtual bool open();
    virtual void close();

    virtual void update();

    virtual bool isFrameNew();
    
    // Video dimension
    virtual int getVideoWidth();
    virtual int getVideoHeight();
    virtual int getDepthWidth();
    virtual int getDepthHeight();

    // Motor functions
    virtual void setAngle(int angleInDegrees);
    virtual int getCurrentAngle();
    virtual int getTargetAngle();

    // RGB pixels
    virtual ofColor getColorAt(int x, int y);
    virtual ofColor getColorAt(const ofPoint & p);

    virtual ofColor getCalibratedColorAt(int depthX, int depthY);
    virtual ofColor getCalibratedColorAt(const ofPoint& depthPoint);

    // Depth data
    virtual unsigned char* getDepthPixels();

    virtual ofVec3f getWorldCoordinateAt(int depthX, int depthY);

    virtual unsigned short getDistanceAt(int depthX, int depthY);
    virtual unsigned short getDistanceAt(const ofPoint& depthPoint);
    
    // Calibration data
    virtual void setVirtualTranslation(float x, float y, float z);
    virtual void setVirtualRotation(float x, float y, float z);

    // Label data
    virtual void disableUserSwitch();
    virtual void enableUserSwitch();
    virtual int getTrackedPlayer();
    virtual int getClosestPlayer();
    virtual float getTrackedPlayerDepthAt(int depthX, int depthY);
    virtual int getPlayerIndexAt(int depthX, int depthY);
    virtual int getPlayerIndexAt(const ofPoint& point);

    // Skeleton data
    virtual bool isFoundSkeleton();
    virtual void drawSkeleton(int x, int y, int w, int h);
    virtual const ofPoint* getSkeleton();
    virtual ofPoint* getVirtualSkeleton();
    virtual ofPoint* getWorldSkeleton();
    virtual ofVec3f getHeadPosition();
    virtual ofVec3f getLeftWrist();
    virtual ofVec3f getRightWrist();
    virtual float getLeftArmElevation();
    virtual float getRightArmElevation();
	//virtual ofVec3f transformDepthToVirtualWorld(int x, int y);

    // Drawing functions
    virtual void drawDepth(float x, float y, float w, float h);
    virtual void drawRaw(float x, float y, float w, float h);

};

#endif
#endif