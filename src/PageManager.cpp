#include "kinect\KinectNui.h"
#include "PageManager.h"


/*
// set up the view timer
    ofAddListener(viewTimer.TIMER_REACHED, this, &ViewManager::onTimerReached);
    viewTimer.setup(ControlsFactory::controlsGeneral()->autoSwitchDelay_logo() * 1000, false);
    viewTimer.stopTimer();
*/

PageManager::PageManager(void) {

	_calibX = 295;
	_calibY = 214;
	_calibMin = 500;
	_calibMax = 800;

	_storyState = STORY_NONE;
	_censorState = CENSOR_NONE;
	_visitorState = VISITOR_NONE;
	_pageState = PAGE_DOWN;

	_timeCensorNone = 27300;
	_timeCensorLittle = 22000;
	_timeCensorMore = 27000;
	_timeCensorAll = 2000;
	_timeQuestion = 3000;
	_timeNewspaper = 600;
	_timeStandby = 15000;

	_imagesNowNb = 15;

	ofAddListener(_timerStoryboard.TIMER_REACHED, this, &PageManager::onStoryboardTimerReached);

    //ofAddListener(timerCensorNone.TIMER_REACHED, this, &PageManager::startLittleCensor);
    //ofAddListener(timerCensorLittle.TIMER_REACHED, this, &PageManager::startMoreCensor);
    //ofAddListener(timerCensorMore.TIMER_REACHED, this, &PageManager::startAllCensor);
    //ofAddListener(timerCensorAll.TIMER_REACHED, this, &PageManager::startQuestion);
    //ofAddListener(timerQuestion.TIMER_REACHED, this, &PageManager::startNewspapers);
	//ofAddListener(timerNewspapers.TIMER_REACHED, this, &PageManager::startNewspapers);
    //ofAddListener(timerPauseExperience.TIMER_REACHED, this, &PageManager::resetExperience);

	_storyNbPages = 6;

}

PageManager::~PageManager(void) {
}

void PageManager::reset() {
	
	_censorState = CENSOR_NONE;
	_visitorState = VISITOR_NONE;
	_pageState = PAGE_DOWN;
	
	_currentPage = 0;
	_currentLeftPage = NULL;//&_leftPageImages[_currentPage][_censorState];
	_currentRightPage =  NULL;//&_rightPageImages[_currentPage][_censorState];

	_hasBookTexture = false;
	_currentBookTexture = ofTexture();

	_imagesNowIdx = 0;
	_imagesNowNb = 15;
	
}

void PageManager::setup() {

	_audioManager.setup();

	_kinect = dynamic_cast<KinectNui*>(KinectFactory::kinect());
	
	for (int i=0 ; i<_imagesNowNb ; i++) {
		string fileName = ofToDataPath("now/") + ofToString(i+1) + ".jpg";
		_imagesNow[i].loadImage(fileName);
	}

	_image2013.loadImage(ofToDataPath("2013-noir.png"));

	// NONE - 000
	string pageName = "";
	for (int i=0 ; i<_storyNbPages ; i++) {
		pageName = ofToDataPath("pages/000/") + ofToString(i+1) + "-verso.png";
		_leftPageImages[i][0].loadImage(pageName);

		pageName = ofToDataPath("pages/000/") + ofToString(i+1) + "-recto.png";
		_rightPageImages[i][0].loadImage(pageName);
	}
	// LITTLE - 001
	for (int i=0 ; i<_storyNbPages ; i++) {
		pageName = ofToDataPath("pages/001/") + ofToString(i+1) + "-verso.png";
		_leftPageImages[i][1].loadImage(pageName);

		pageName = ofToDataPath("pages/001/") + ofToString(i+1) + "-recto.png";
		_rightPageImages[i][1].loadImage(pageName);

	}
	// MORE - 002
	for (int i=0 ; i<_storyNbPages ; i++) {
		pageName = ofToDataPath("pages/002/") + ofToString(i+1) + "-verso.png";
		_leftPageImages[i][2].loadImage(pageName);
		
		pageName = ofToDataPath("pages/002/") + ofToString(i+1) + "-recto.png";
		_rightPageImages[i][2].loadImage(pageName);
	}
	reset();
	
}

ofImage* PageManager::getLeftPage() {
	return _currentLeftPage;
}

ofImage* PageManager::getRightPage() {
	return _currentRightPage;
}

ofTexture PageManager::getBookTexture() {
	if (_hasBookTexture)
		return _currentBookTexture;
	else
		return ofTexture();
}

void PageManager::update() {

	// Gestion du visiteur
	if (_storyState == STORY_NONE) {
		if (_visitorState == VISITOR_NONE) {
			if (_kinect->isFoundSkeleton()) {
				// Un visiteur vient d'arriver, on demarre l'experience
				_visitorState = VISITOR_PRESENT;
				startExperience();
				ofLog() << "START EXPERIENCE";
			}
		}
	}

	// Gestion de la page
	// Si la page est levee, 

	if (_storyState == STORY_CENSORSHIP && _censorState != CENSOR_ALL) {
		
		// On verifie la profondeur en un point
		unsigned short depth = _kinect->getDistanceAt(_calibX, _calibY); 
		
		if (_pageState == PAGE_DOWN) {
			if (depth > _calibMin && depth < _calibMax) {
				// la page vient de se lever
				ofLog() << "PAGE DEBOUT";
				_pageState = PAGE_UP;
			} else {
				// rien
				_pageState = PAGE_DOWN;
			}
		} else if (_pageState == PAGE_UP) {
			if (depth > _calibMin && depth < _calibMax) {
				// la page est deja debout
				//ofLog() << "PAGE ENCORE DEBOUT";
				_currentLeftPage = &_leftPageImages[_currentPage][_censorState];
				_currentRightPage = &_rightPageImages[(_currentPage+1)%_storyNbPages][_censorState];
			} else {
				// la page vient de tourner
				ofLog() << "PAGE TOURNEE";
				_pageState = PAGE_DOWN;
				_currentPage = (_currentPage+1)%_storyNbPages;
				_currentLeftPage = &_leftPageImages[_currentPage][_censorState];
				_currentRightPage = &_rightPageImages[_currentPage][_censorState];
			}
		}
	}
}

void PageManager::startExperience() {
	// start everything
	_storyState = STORY_CENSORSHIP;
	_censorState = CENSOR_NONE;
	
	_timerStoryboard.setup(_timeCensorNone,false);
	ofLog() << "[STORY] STORY_NONE --> STORY_CENSORSHIP";

	_currentLeftPage = &_leftPageImages[_currentPage][_censorState];
	_currentRightPage = &_rightPageImages[_currentPage][_censorState];
	
	_audioManager.startTrameSonore();
}


#pragma mark - Timer event handlers
//--------------------------------------------------------------

//ofxTimer timerStoryboard;
void PageManager::onStoryboardTimerReached(const void* sender, ofEventArgs& args) {
	
	_timerStoryboard.stopTimer();

	//_storyState = STORY_NONE;
	//_censorState = CENSOR_NONE;
	//_visitorState = VISITOR_NONE;
	//_pageState = PAGE_DOWN;

	if (_storyState > STORY_NONE) {

		if (_storyState == STORY_CENSORSHIP ) {
			if (_censorState == CENSOR_NONE) {
				ofLog() << "\t[STORY_CENSORSHIP] CENSOR_NONE --> CENSOR_LITTLE";
				_storyState = STORY_CENSORSHIP;
				_censorState = CENSOR_LITTLE;

				_timerStoryboard.setup(_timeCensorLittle,false);
			}
			else if (_censorState == CENSOR_LITTLE) {
				ofLog() << "\t[STORY_CENSORSHIP] CENSOR_LITTLE --> CENSOR_MORE";
				_storyState = STORY_CENSORSHIP;
				_censorState = CENSOR_MORE;

				_timerStoryboard.setup(_timeCensorMore,false);
			}
			else if (_censorState == CENSOR_MORE) {
				ofLog() << "\t[STORY_CENSORSHIP] CENSOR_MORE --> CENSOR_ALL";
				_storyState = STORY_CENSORSHIP;
				_censorState = CENSOR_ALL;
				
				// cancel images
				_currentLeftPage = NULL;
				_currentRightPage = NULL;
				
				_timerStoryboard.setup(_timeCensorAll,false);
			}
			else if (_censorState == CENSOR_ALL) {

				ofLog() << "\t[STORY_CENSORSHIP] CENSOR_ALL --> X";
				ofLog() << "[STORY] STORY_CENSORSHIP --> STORY_QUESTION";
				_storyState = STORY_QUESTION;
				_censorState = CENSOR_NONE;
				
				_hasBookTexture = true;
				_currentBookTexture = _image2013.getTextureReference();

				_timerStoryboard.setup(_timeQuestion,false);
			}
		} else if (_storyState == STORY_QUESTION) {
			_storyState = STORY_NOW;
			ofLog() << "[STORY] STORY_QUESTION --> STORY_NOW";
			_timerStoryboard.setup(_timeNewspaper,false);

		} else if (_storyState == STORY_NOW) {
			
			if (_imagesNowIdx == _imagesNowNb) {

				_hasBookTexture = false;
				_currentBookTexture = ofTexture();
				
				ofLog() << "[STORY] STORY_NOW --> STORY_STANDBY";
				_storyState = STORY_STANDBY;
				_timerStoryboard.setup(_timeStandby,false);

			} else {

				_hasBookTexture = true;
				_currentBookTexture = _imagesNow[_imagesNowIdx].getTextureReference();
				_imagesNowIdx++;
				
				_timerStoryboard.setup(_timeNewspaper,false);
			}
		}
		else if (_storyState == STORY_STANDBY) {
			ofLog() << "[STORY] STORY_STANDBY --> STORY_NONE";
			_storyState = STORY_NONE;
				
			_audioManager.stopTrameSonore();
			reset();
		}

	} else {
		_timerStoryboard.stopTimer();
	}
}

/*
void PageManager::startLittleCensor(const void* sender, ofEventArgs& args) {
	_censorState = CENSOR_LITTLE;
	timerCensorLittle.setup(_timeCensorLittle*1000,false);
}

void PageManager::startMoreCensor(const void* sender, ofEventArgs& args) {
	_censorState = CENSOR_MORE;
	timerCensorMore.setup(_timeCensorMore*1000,false);
}

void PageManager::startAllCensor(const void* sender, ofEventArgs& args) {
	_censorState = CENSOR_ALL;
	_currentLeftPage = NULL;
	_currentRightPage = NULL;
	timerCensorAll.setup(_timeCensorAll*1000,false);
}

void PageManager::startQuestion(const void* sender, ofEventArgs& args) {
	_storyState = STORY_QUESTION;
	timerQuestion.setup(_timeQuestion*1000,false);
	// Attention le temps par au debut de l'affichage des journaux
	timerPauseExperience.setup(_timePauseExperience*1000,false);
}

void PageManager::startNewspapers(const void* sender, ofEventArgs& args) {
	if (_imagesNowIdx == 0) {
		_storyState = STORY_NOW;
		timerNewspapers.setup(2,true);
		_imagesNowIdx = 1;
	} else {
		_imagesNowIdx++;
		if (_imagesNowIdx == 5) {
			timerNewspapers.stopTimer();
			_audioManager.stopTrameSonore();
			reset();
		}
	}
}
*/
/*
void PageManager::resetExperience(const void* sender, ofEventArgs& args) {
	_audioManager.stopTrameSonore();
	reset();
}
*/