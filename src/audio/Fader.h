/*
 *  Fader.h
 *
 *  Created by gameover on 6/01/12.
 *  Copyright 2012 trace media. All rights reserved.
 *
 */

#include "ofUtils.h"

//--------------------------------------------------------------
//--------------------------------------------------------------
enum FaderType 
{
	FADE_LINEAR,
	FADE_LOG,
	FADE_EXP
};

//--------------------------------------------------------------
//--------------------------------------------------------------
class Fader
{
    public:
        Fader(float from, float to, float over, FaderType type, bool autoStart = true);
        
        void start();
        void update();
        
        inline bool isFading() { return _isFading; }
        inline int getCurrTime() { return _currTime; }
        inline float getValue() { return _value; }
        inline float getTo() { return _to; }
        inline float getFrom() { return _from; }
        
    private:
        void    _calculate();
        
        float   _from;
        float   _to;
        float   _value;
        float   _pct;
        
        int		_over;
        int		_startTime;
        int     _currTime;
        int     _now;
        
        int     _type;
        bool    _isFading;
};
