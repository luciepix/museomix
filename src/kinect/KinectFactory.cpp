#ifdef WIN32
    #include <windows.h>
    #include <NuiApi.h>
    #include "KinectNui.h"
#else
    #include "KinectFreenect.h"
#endif

#include "KinectFactory.h"

//--------------------------------------------------------------
KinectInterface * KinectFactory::_kinect = NULL;

//--------------------------------------------------------------
KinectInterface* KinectFactory::kinect() 
{
    if (_kinect == NULL) {
#ifdef WIN32
        _kinect = new KinectNui();
#else
        _kinect = new KinectFreenect();
#endif
    }
    return _kinect;
}
