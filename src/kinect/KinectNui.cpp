#ifdef WIN32

#include "KinectNui.h"
//#include "Settings.h"
//#include "ControlsFactory.h"

KinectNui::KinectNui() {
    
    _trackedSkeletonID = -1;
    _closestSkeletonID = -1;
    _highestScoreSkeletonID = -1;
    _trackedPlayerID = -1;
    _closestPlayerID = -1;

    _lockOnUser = false;
    
    _switchActivatedSince = 0;
    _switchDelayInSeconds = 3;
    _switchActivated = false;
    
    _camVirtualPose.newIdentityMatrix();
    _camVirtualRotation.newIdentityMatrix();
    _camVirtualTranslation.set(0,0,0);

    _nbFrameToConsiderNotFound = 15;
    _nbFrameSkeletonNotFound = _nbFrameToConsiderNotFound;

    _doSmooth = false;
}

// Device management functions
bool KinectNui::init() {
	bool result = _kinect.init(false, true, false, false, true, false, false, 
		NUI_IMAGE_TYPE_COLOR, 
        NUI_IMAGE_RESOLUTION_640x480, NUI_IMAGE_RESOLUTION_320x240);

	if (result) {
		_depthDraw = ofxKinectNuiDrawTexture::createTextureForDepth(_kinect.getDepthResolution());
		_kinect.setDepthDrawer(_depthDraw);

		_skeletonDraw = new ofxKinectNuiDrawSkeleton();
		_kinect.setSkeletonDrawer(_skeletonDraw);
	}

	return result;
}

bool KinectNui::open() {
	
	_kinect.setNearmode(true);

    bool result = _kinect.open();

	if (result) {
		_kinect.useSeatedSkeleton();
	}

	return result;
}

void KinectNui::close() {
	if(_skeletonDraw) {
		delete _skeletonDraw;
		_skeletonDraw = NULL;
	}
	if(_depthDraw) {
		_depthDraw->destroy();
		_depthDraw = NULL;
	}
    _kinect.close();
}

void KinectNui::update() {

    // Reset les poids des squelettes
    for (int i=0 ; i<kinect::nui::SkeletonFrame::SKELETON_COUNT ; i++) 
        _skeletonWeights[i] = 0.0;

    // Si on a plus de squelette, on ne smooth plus les donnes
    // On le fait avant de faire l'update pcq on veut le statut
    // de la derniere frame.
    _doSmooth = isFoundSkeleton();

    // Mets a jour les donnes de la Kinect
    _kinect.update();
    
    if (!_kinect.isFoundSkeleton())
        _nbFrameSkeletonNotFound++;

    // Mise-a-jour de l'information de suivi de squelette
    // Toutes les jointures de tous les squelettes
    _kinect.getSkeletonPoints(_skeletons);
    
    //updateWorldSkeleton();
    //updateVirtualSkeleton();
    //updateTracking();
    

}

ofPoint KinectNui::transformSkeletonToDepthImage(ofVec3f joint) {
    
    //if(joint == NULL) {
    //    return ofPoint(-1,-1);
    //}
    
    //
    // Requires a valid depth value.
    //
    int depthX = -1;
    int depthY = -1;

    if(joint.z > FLT_EPSILON) {

        DWORD width = _kinect.getDepthResolutionWidth();
        DWORD height = _kinect.getDepthResolutionHeight();

        //
        // Center of depth sensor is at (0,0,0) in skeleton space, and
        // and (width/2,height/2) in depth image coordinates.  Note that positive Y
        // is up in skeleton space and down in image coordinates.
        //
        // The 0.5f is to correct for casting to int truncating, not rounding

        depthX = static_cast<INT>( width / 2 + joint.x * (width/320.f) * NUI_CAMERA_SKELETON_TO_DEPTH_IMAGE_MULTIPLIER_320x240 / joint.z + 0.5f);
        depthY = static_cast<INT>( height / 2 - joint.y * (height/240.f) * NUI_CAMERA_SKELETON_TO_DEPTH_IMAGE_MULTIPLIER_320x240 / joint.z + 0.5f);
        
        //
        //  Depth is in meters in skeleton space.
        //  The depth image pixel format has depth in millimeters shifted left by 3.
        //
        //*pusDepthValue = static_cast<USHORT>(vPoint.z *1000) << 3;

    } else {
        depthX = -1;
        depthY = -1;
    }

    return ofPoint(depthX, depthY);
}



bool KinectNui::isFrameNew() {
    return _kinect.isFrameNew();
}

// Video dimension
int KinectNui::getVideoWidth() {
    return _kinect.getVideoResolutionWidth();
}

int KinectNui::getVideoHeight() {
    return _kinect.getVideoResolutionHeight();
}

int KinectNui::getDepthWidth() {
    return _kinect.getDepthResolutionWidth();
}

int KinectNui::getDepthHeight() {
    return _kinect.getDepthResolutionHeight();
}

// Motor functions
void KinectNui::setAngle(int angleInDegrees) {
    _kinect.setAngle(angleInDegrees);
}

int KinectNui::getCurrentAngle() {
    return _kinect.getCurrentAngle();
}

int KinectNui::getTargetAngle() {
    return _kinect.getTargetAngle();
}

// RGB pixels
ofColor KinectNui::getColorAt(int x, int y) {
    return _kinect.getColorAt(x,y);
}

ofColor KinectNui::getColorAt(const ofPoint & p) {
    return _kinect.getColorAt(p);
}

ofColor KinectNui::getCalibratedColorAt(int depthX, int depthY) {
    return _kinect.getCalibratedColorAt(depthX, depthY);
}

ofColor KinectNui::getCalibratedColorAt(const ofPoint& depthPoint) {
    return _kinect.getCalibratedColorAt(depthPoint);
}

// Depth data
unsigned char* KinectNui::getDepthPixels() {
    return _kinect.getDepthPixels().getPixels();
}

ofVec3f KinectNui::getWorldCoordinateAt(int depthX, int depthY) {
    return _kinect.getWorldCoordinateFor(depthX, depthY);
}

unsigned short KinectNui::getDistanceAt(int depthX, int depthY) {
    return _kinect.getDistanceAt(depthX, depthY);
}

unsigned short KinectNui::getDistanceAt(const ofPoint& depthPoint) {
    return _kinect.getDistanceAt(depthPoint);
}

// Calibration data
void KinectNui::setVirtualTranslation(float x, float y, float z) {

    _camVirtualTranslation.set(x,y,z);

    //updateVirtualPose();
}

void KinectNui::setVirtualRotation(float x, float y, float z) {

    // on a seulement un tilt
    ofLog(OF_LOG_VERBOSE, "setVirtualRotation() %f %f %f", x, y, z);
    //_camVirtualRotation.newRotationMatrix(x, 1.0, 0.0, 0.0);
    _camVirtualRotation.makeRotationMatrix(x, 1.0, 0.0, 0.0);
    ofLog(OF_LOG_VERBOSE, "setVirtualRotation()\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f",
        _camVirtualRotation._mat[0][0], _camVirtualRotation._mat[0][1], _camVirtualRotation._mat[0][2], _camVirtualRotation._mat[0][3],
        _camVirtualRotation._mat[1][0], _camVirtualRotation._mat[1][1], _camVirtualRotation._mat[1][2], _camVirtualRotation._mat[1][3],
        _camVirtualRotation._mat[2][0], _camVirtualRotation._mat[2][1], _camVirtualRotation._mat[2][2], _camVirtualRotation._mat[2][3],
        _camVirtualRotation._mat[3][0], _camVirtualRotation._mat[3][1], _camVirtualRotation._mat[3][2], _camVirtualRotation._mat[3][3] );

    //updateVirtualPose();
}

// Label data
void KinectNui::disableUserSwitch() {
    _lockOnUser = true;
}

void KinectNui::enableUserSwitch() {
    _lockOnUser = false;
}


int KinectNui::getTrackedPlayer() {
    return _trackedSkeletonID;
}

int KinectNui::getClosestPlayer() {
    return _closestSkeletonID;
}

float KinectNui::getTrackedPlayerDepthAt(int depthX, int depthY) {
    
    // S'il n'y a pas de tracking en cours, il est inutile
    // de calculer la profondeur
    if (_trackedPlayerID <= 0 || _trackedSkeletonID < 0 )
        return -1.0f;
    
    static float _minDepth = 99999999;
    static float _maxDepth = 0;
    
    int idx = _kinect.getPlayerIndexAt(depthX, depthY);
    
    if (idx == _trackedPlayerID) {
        float depth = _kinect.getDistanceAt(depthX,depthY);
        _minDepth = MIN(_minDepth, depth);
        _maxDepth = MAX(_maxDepth, depth);
        return ofMap(depth, _minDepth, _maxDepth, 0, 1);
    }

    return -1.0f;
}

int KinectNui::getPlayerIndexAt(int depthX, int depthY) {
    return _kinect.getPlayerIndexAt(depthX, depthY);
}

int KinectNui::getPlayerIndexAt(const ofPoint& point) {
    return _kinect.getPlayerIndexAt(point);
}

bool KinectNui::isFoundSkeleton() {
    
    if (_kinect.isFoundSkeleton()) {
        _nbFrameSkeletonNotFound = 0;
        return true;
    }

    if (_nbFrameSkeletonNotFound < _nbFrameToConsiderNotFound) {
        return true;
    }

    return false;
}

void KinectNui::drawSkeleton(int x, int y, int w, int h) {
    _kinect.drawSkeleton(x,y,w,h);
	
    ofFill();
    if (isFoundSkeleton()) {
        ofSetColor(0,255,0);
        ofCircle(x, y, 10);
    } else {
        ofSetColor(255,0,0);
        ofCircle(x, y, 10);
    }

    ofNoFill();

    ofVec3f head3d;
    ofVec3f head2d;
    if (_trackedSkeletonID >= 0) {
        head3d = _skeletons[_trackedSkeletonID][NUI_SKELETON_POSITION_HEAD];
        head2d = transformSkeletonToDepthImage(head3d);

        ofVec2f drawHead;
        drawHead.x = (head2d.x)/_kinect.getDepthResolutionWidth()*w + x;
        drawHead.y = (head2d.y)/_kinect.getDepthResolutionHeight()*h + y;

        ofSetLineWidth(3);
        ofSetColor(255,0,0);
        ofCircle(drawHead, 30);
    }

    if (_highestScoreSkeletonID >= 0) {
        head3d = _skeletons[_highestScoreSkeletonID][NUI_SKELETON_POSITION_HEAD];
        head2d = transformSkeletonToDepthImage(head3d);
        
        ofVec2f drawHead;
        drawHead.x = (head2d.x)/_kinect.getDepthResolutionWidth()*w + x;
        drawHead.y = (head2d.y)/_kinect.getDepthResolutionHeight()*h + y;

        ofSetLineWidth(3);
        ofSetColor(0,255,0);
        ofCircle(drawHead, 25);

    }
}

const ofPoint* KinectNui::getSkeleton() {
    
    if (_trackedSkeletonID < 0) {
        //ofLogWarning() << "No skeleton available!";
        return NULL;
    }

    return _skeletons[_trackedSkeletonID];
}

ofPoint* KinectNui::getVirtualSkeleton() {
    
    if (_trackedSkeletonID < 0) {
        //ofLogWarning() << "No skeleton available!";
        return NULL;
    }

    return _virtualSkeletons[_trackedSkeletonID];
}

ofPoint* KinectNui::getWorldSkeleton() {
    
    if (_trackedSkeletonID < 0) {
        //ofLogWarning() << "No skeleton available!";
        return NULL;
    }

    return _worldSkeletons[_trackedSkeletonID];
}

// Skeleton data
ofVec3f KinectNui::getHeadPosition() {
    if (isFoundSkeleton()) {
		return _head;
    }
    // Aucun squelette n'a ete trouve dans cette image
    return ofVec3f(0.0f,0.0f,-1.0f);
}

ofVec3f KinectNui::getLeftWrist() {
    if (isFoundSkeleton()) {
        return _leftHand;
    }
    // Aucun squelette n'a ete trouve dans cette image
    return ofVec3f(0.0f,0.0f,-1.0f);
}

ofVec3f KinectNui::getRightWrist() {
    if (isFoundSkeleton()) {
        return _rightHand;
    }
    // Aucun squelette n'a ete trouve dans cette image
    return ofVec3f(0.0f,0.0f,-1.0f);
}


float KinectNui::getLeftArmElevation() {
    ofVec3f shoulder;
    ofVec3f vArm;
    float armAngle;
    float x,y,z;

    if (isFoundSkeleton()) {
        ofVec3f vY = ofVec3f(0,1,0);

        shoulder = _virtualSkeletons[_trackedSkeletonID][NUI_SKELETON_POSITION_SHOULDER_LEFT];
        vArm = _leftHand - shoulder;
        armAngle = acos( vArm.dot(vY) / (vArm.length()*vY.length()) );
        return ofMap(armAngle, 0, PI, 0, 1);
    }
    // Aucun squelette n'a ete trouve dans cette image
    return -1.0f;
}

float KinectNui::getRightArmElevation() {
    ofVec3f shoulder;
    ofVec3f vArm;
    float armAngle;
    float x,y,z;

    if (isFoundSkeleton()) {
        ofVec3f vY = ofVec3f(0,1,0);

        shoulder = _virtualSkeletons[_trackedSkeletonID][NUI_SKELETON_POSITION_SHOULDER_RIGHT];
        vArm = _rightHand - shoulder;
        armAngle = acos( vArm.dot(vY) / (vArm.length()*vY.length()) );
        /*
        ofLog(OF_LOG_VERBOSE, "ARM RIGHT: (%0.02f,%0.02f,%0.02f)-(%0.02f,%0.02f,%0.02f) = (%0.02f,%0.02f,%0.02f) --> %f", 
            _rightHand.x, _rightHand.y, _rightHand.z,
            shoulder.x, shoulder.y, shoulder.z, 
            vArm.x, vArm.y, vArm.z, armAngle);
        
        ofLog(OF_LOG_VERBOSE, "\t: acos( %0.02f / (%0.02f*%0.02f) ) --> %f -> %f", 
            vArm.dot(vY), vArm.length(), vY.length(), armAngle, ofMap(armAngle, 0, PI, 0, 1));
        */
        return ofMap(armAngle, 0, PI, 0, 1);
    }
    // Aucun squelette n'a ete trouve dans cette image
    return -1.0f;
}

// Drawing functions
void KinectNui::drawDepth(float x, float y, float w, float h) {
    
    ofSetColor(255);
    
    _kinect.drawDepth(x, y, w, h);
	if (_kinect.grabsLabel())
		_kinect.drawLabel(x,y,w,h);
    
    // draw a frame
    ofNoFill();
    ofRect(x, y, w, h);
    ofFill();
}

void  KinectNui::drawRaw(float x, float y, float w, float h) {
    
    ofSetColor(255);
    
    //_kinect.draw(x, y, w, h);
	_kinect.drawSkeleton(x, y, w, h);
    
    // draw a frame
    ofNoFill();
    ofRect(x, y, w, h);
    ofFill();
}

#endif