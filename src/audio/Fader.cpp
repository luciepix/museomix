//
//  Fader.cpp
//  OffAxisPerspectiveProjection
//
//  Created by Elie Zananiri on 12-05-19.
//  Copyright (c) 2012 Departement. All rights reserved.
//

#include "Fader.h"

//--------------------------------------------------------------
Fader::Fader(float from, float to, float over, FaderType type, bool autoStart)
    : _from(from)
    , _to(to)
    , _over(over)
    , _type(type)
    , _isFading(false)
{
    ofLogVerbose("Fader") << "Creating fade from " << from << " to " << to << " over " << over << " ms";
    if (autoStart) start();
}

//--------------------------------------------------------------
void Fader::start() 
{
    if (_isFading) return;
    
    ofLogVerbose("Fader") << "Starting fade";
    
    _isFading = true;
    _startTime = ofGetElapsedTimeMillis();
    _currTime = 0;
    _value = _from;
}

//--------------------------------------------------------------
void Fader::update() 
{
    if (!_isFading) return;
    
    _calculate();
    
    // check if the Fader is done
    if (_currTime >= _over) {
        _value = _to; 
        _currTime = _over;
        
        _isFading = false;
    }
}

//--------------------------------------------------------------
void Fader::_calculate() 
{    
    _now = ofGetElapsedTimeMillis() - _startTime;
    
    switch (_type) {
        case FADE_LINEAR:
        {
            _pct = 1.0 - ((float)_now / (float)_over);
            break;
        }
            
        case FADE_LOG:
        {
            float base = 10; //2.71828182845904523536028747135266249775724709369995 use log instead of log10;
            _pct = log10((float)_over / ((float)_over + (base-1) * (float)_now)) + 1;
            break;
        }
            
        case FADE_EXP:
        {
            float base = 10.0f; // not really sure why I need the 1.039...
            _pct = -pow((double)base, (double)(((float)_now * 1.039 - (float)_over)/(float)_over)) + 1.099;
            break;
        }
    }
    
    _value = (_pct * (_from - _to) + _to);
    _currTime  = _now;
    
    //ofLogVerbose("Fader") << _now << " " << _pct << " " << _from << " " << _to << "  " << _fade.value;
}
