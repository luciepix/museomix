#ifndef DPT_KINECT_FACTORY_H
#define DPT_KINECT_FACTORY_H

#pragma once

#include "KinectInterface.h"

//--------------------------------------------------------------
//--------------------------------------------------------------
class KinectFactory
{
    public:
        KinectFactory(void) {};
        ~KinectFactory(void) {};

        static KinectInterface * kinect();
        
    private:
        static KinectInterface *_kinect;
};


#endif